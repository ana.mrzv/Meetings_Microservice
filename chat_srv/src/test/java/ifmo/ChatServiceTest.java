package ifmo;

import ifmo.dto.MessageDTO;
import ifmo.model.ChatEntity;
import ifmo.model.MessageEntity;
import ifmo.repository.ChatRepository;
import ifmo.repository.MessageRepository;
import ifmo.service.ChatService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
public class ChatServiceTest {

    @Autowired
    ChatService chatService;

    @MockBean
    MessageRepository messageRepository;

    @MockBean
    ChatRepository chatRepository;

    @DisplayName("Get all messages")
    @Test
    public void getAllMessages(){
        var chatEntity = new ChatEntity();
        chatEntity.setId(1L);
        chatEntity.setCreationDate(LocalDateTime.of(LocalDate.of(2024, 9, 23), LocalTime.of(12, 5)));

        var messageEntity1 = new MessageEntity();
        messageEntity1.setId(1L);
        messageEntity1.setContent("приветик");

        var messageEntity2 = new MessageEntity();
        messageEntity2.setId(2L);
        messageEntity2.setContent("покасик");

        List<MessageEntity> messages = new ArrayList<>();
        messages.add(messageEntity1);
        messages.add(messageEntity2);

        Mockito.when(chatRepository.getChatEntityById(1L)).thenReturn(Optional.of(chatEntity));
        Mockito.when(messageRepository.findAllByChat(chatEntity)).thenReturn(List.of(messageEntity1, messageEntity2));

        Assertions.assertEquals(chatService.getAllMessagesByChatId(1L), messages.stream().map(MessageDTO::new).collect(Collectors.toList()));
    }
}
